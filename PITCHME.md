# Language Intelligence
A framework for rapid language-intelligence demo development

#HSLIDE

# Language Intelligence

1. Transformers: Augment a Knowledge Graph
2. Pipelines: Create the fabric to support knowledge
3. Utility Sets: Build an end-to-end knowledge base
4. API: Access the knowledge Graph
5. Interfaces: Interact with the Knowledge Graph

#HSLIDE
